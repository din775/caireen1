import jira
from jira import JIRA
import re
from datetime import datetime,timedelta
from tqdm import tqdm

options = {
    'server': 'https://sd.mutantbr.com'
    }
    
jira = JIRA(options,basic_auth=('user', 'pass')) 


falha_fato = [
{
    'project': {'id': '10400'},
    'summary': 'Falha Fato Teste',
    'description': 'Criticidade do alarme: Alta\nEquipe responsável: Banco de Dados\nFrequencia de atualização: Todo Minuto 15 e Minuto 45\nJanela de monitoração: Contínuo (24x7) \nFuncionamento: Verifica falhas no Termometro SKY Hora a Hora \nTroubleshooting: A Equipe de Banco de Dados devera ser acionada para a tratativa do alarme. ',
    'issuetype': {'name': 'Incidente'},
    'customfield_10313': {'id':'12007'},
    'customfield_10304':'XXXXXXXXXX',
    'priority': {'id':'5'}
},

{
    'project': {'id': '10400'},
    'summary': 'Falha Fato Teste de fora',
    'description': 'Criticidade do alarme: Alta\nEquipe responsável: Banco de Dados\nFrequencia de atualização: Todo Minuto 15 e Minuto 45\nJanela de monitoração: Contínuo (24x7) \nFuncionamento: Verifica falhas no Termometro SKY Hora a Hora \nTroubleshooting: A Equipe de Banco de Dados devera ser acionada para a tratativa do alarme. ',
    'issuetype': {'id': '10107'},
    'customfield_10304':'XXXXXXXXXXXXX',
    'priority':{'id':'5'},
    'assignee':{'name':'laravel.padilla'},
    'customfield_10128':{'id':'10061'},
    'customfield_10503':{'id':'10508'}
}]

issues = jira.create_issues(field_list=falha_fato)
jira.assign_issue(issues[0]['issue'].key, 'reginaldo.carvalho')
jira.transition_issue(issues[0]['issue'].key,'11',comment='Chamado aberto para monitoria')
jira.issue(issues[0]['issue'].key).add_field_value('customfield_10103',13)


jira.create_issue_link(
    type="Duplicate",
    inwardIssue='URA-47023',
    outwardIssue=issues[0]['issue'].key
    }
)



# create_nissue = jira.create_issue(project = 'URA', summary= 'Teste Smiles', description = 'Criticidade do Alarme (Zabbix): Desastre \nEquipe Resp: TI \nJanela de Monitoria: Contínuo (24x7)\nFuncionamento: Monitora o horário do sistema operacional, alarma se 5min para + ou para -\nTroubleshootings: Acionar a equipe de TI para que verifiquem o problema', issuetype={'name':'Incidente'},
#     customfield_10313={'id':'12007'}, customfield_10304='(11)3094-2134/(11)4861-4630', priority={'id':'10000'})

# jira.assign_issue(create_nissue,'laravel.padilla')

# jira.transition_issue(create_nissue,'11',comment='a')