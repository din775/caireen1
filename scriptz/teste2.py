import jira
from jira import JIRA

options = {
    'server': 'https://sd.mutantbr.com'
    }
    
jira = JIRA(options,basic_auth=('user', 'pass'))   

jira.create_issue_link(
    type="Duplicate",
    inwardIssue='URA-47023',
    outwardIssue='IVR-65977' )

jira.create_issue_link(
    type="Duplicate",
    inwardIssue='URA-47024',
    outwardIssue='IVR-65977' )

jira.create_issue_link(
    type="Duplicate",
    inwardIssue='URA-47030',
    outwardIssue='IVR-65977' )

jira.create_issue_link(
    type="Duplicate",
    inwardIssue='URA-47032',
    outwardIssue='IVR-65977' )

jira.create_issue_link(
    type="Duplicate",
    inwardIssue='URA-47031',
    outwardIssue='IVR-65977' )

jira.create_issue_link(
    type="Duplicate",
    inwardIssue='URA-47027',
    outwardIssue='IVR-65977' )

jira.create_issue_link(
    type="Duplicate",
    inwardIssue='URA-47028',
    outwardIssue='IVR-65977' )

jira.create_issue_link(
    type="Duplicate",
    inwardIssue='URA-47026',
    outwardIssue='IVR-65977' )

jira.create_issue_link(
    type="Duplicate",
    inwardIssue='URA-47025',
    outwardIssue='IVR-65977' )

jira.create_issue_link(
    type="Duplicate",
    inwardIssue='URA-47029',
    outwardIssue='IVR-65977' )

