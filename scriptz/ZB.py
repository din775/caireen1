from jira import JIRA
import pprint
import datetime
from pyzabbix import ZabbixAPI

# The hostname at which the Zabbix web interface is available
ZABBIX_SERVER = 'https://zabbix.mutantbr.com/zabbix'

zapi = ZabbixAPI(ZABBIX_SERVER)

# Login to the Zabbix API
zapi.login('user', 'pass')


options = {
    'server': 'https://sd.mutantbr.com'
    }
    
jira = JIRA(options,basic_auth=('user', 'pass')) 



agora = datetime.datetime.now()
dt2 = datetime.timedelta(hours=4)
menos = agora - dt2
menos2 = menos.timestamp()

zapi.trigger.get

# Get a list of all issues (AKA tripped triggers)
triggers = zapi.trigger.get(only_true=1,
                            skipDependent=1,
                            monitored=1,
                            active=1,
                            output='extend',
                            expandDescription=1,
                            selectHosts=['host'],
                            min_severity=4,
                            lastChangeTill=menos2,
                            )

# Do another query to find out which issues are Unacknowledged
unack_triggers = zapi.trigger.get(only_true=1,
                                  skipDependent=1,
                                  monitored=1,
                                  active=1,
                                  output='extend',
                                  expandDescription=1,
                                  selectHosts=['host'],
                                  withLastEventUnacknowledged=1,
                                  sortfield=['lastchange'],
                                  )
unack_trigger_ids = [t['triggerid'] for t in unack_triggers]
for t in triggers:
    t['unacknowledged'] = True if t['triggerid'] in unack_trigger_ids \
        else False

#Print a list containing only "tripped" triggers
for t in triggers:
    if int(t['value']) == 1:
        eventz = zapi.event.get(objectids=t['triggerid'],#objectids = trigger ID
                        acknowledged=True,
                        select_acknowledges=['message'])

    issue = eventz[-1].get('acknowledges')[-1].get('message')

    print("{0} - {1} {2}----- {3}".format(t['hosts'][0]['host'],
                                     t['description'],
                                     '----(Não reconhecido)' if t['unacknowledged'] else '', issue )                          
              )
    print ('_______________________________________________________________________________________________')