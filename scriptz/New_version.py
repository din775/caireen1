import jira
from jira import JIRA
import re
from datetime import datetime,timedelta
from tqdm import tqdm

options = {
    'server': 'https://sd.mutantbr.com'
    }
    
jira = JIRA(options,basic_auth=('user', 'pass'))


Query = '(project in (URA, MOB, SR, GRAV) AND issuetype in (Incidente) AND status in (resolvido, fechado) AND "Categoria Incidente URA" in ("OPERADORA - FALHA NA ENTREGA DE CHAMADAS", "OPERADORA - OSCILAÇÃO DE LINKS") OR "Categoria Incidente" in ("OPERADORA - FALHA NA ENTREGA DE CHAMADAS", "OPERADORA - OSCILAÇÃO DE LINKS", "OPERADORA - FALHA NO LINK DE ACESSO AO SITE") OR Diagnóstico in ("OSCILAÇÃO DE LINKs E1", "INDISPONIBILIDADE/MAU FUNCIONAMENTO LINKs E1s", "CANAIS BLOQUEADOS", "CANAIS TRAVADOS") OR "Diagnóstico Incidente" in ("OSCILAÇÃO DE LINKs E1", "INDISPONIBILIDADE/MAU FUNCIONAMENTO LINKs E1s", "CANAIS BLOQUEADOS", "CANAIS TRAVADOS")) AND created >= 2019-01-10 AND created <= 2019-01-11 AND Organizations = NET ORDER BY created ASC'



with open(r'C:\Users\guilherme.fagundes.NSXBR\Desktop\OSC.csv','w') as csvfile:

    
     for issue in tqdm(jira.search_issues(Query, maxResults=50)):
        teste1 = re.finditer(r'([(SP|RJO|BHZ|REC|FEC|OLI|POA|BRI)]+[(NET|NETEBT|NETAEC)]+[(UR|GRV)]+([0-9]{1,3}))', issue.fields.description, flags = re.MULTILINE|re.DOTALL|re.I)
    
        teste2 = re.finditer(r'(([0-9]{2}:)+([0-9]{1,2}))', issue.fields.description, flags = re.MULTILINE|re.DOTALL|re.I)

        teste3 = issue.fields.resolutiondate or '1900-01-01T00:00:00.000-0200'

        teste3 = datetime.strptime(teste3, '%Y-%m-%dT%H:%M:%S.000-0200') 

        teste3 = teste3.strftime('%d/%m/%Y %H:%M:%S')

        teste5 = issue.fields.created or '1900-01-01T00:00:00.000-0200'

        teste5 = datetime.strptime(teste5, '%Y-%m-%dT%H:%M:%S.000-0200')

        teste5 = teste5.strftime('%d/%m/%Y')

        teste4 = re.finditer(r'([0-9]º)', issue.fields.description.replace(" ", ""), flags = re.MULTILINE|re.DOTALL|re.I)

        teste6 = re.finditer(r'(AMERICANA|ARACAJU|BHZ-AEC|BHZ-ALMA VIVA|FEIRA SANTANA|PORTO ALEGRE|RECIFE|ATENTO|TIVIT|OLINDA)', issue.fields.description, flags = re.MULTILINE|re.DOTALL|re.I)

        # link_list = []
        # for link in teste4:
        #     link_list.append(link[0])

        # time_list = [yesterday,]
        # for t in teste2:
        #     time_list.append(t[0])

        # server_list = []
        # for servers in teste1:
        #     server_list.append(servers[0])

        #print ("{} ; {} ; {} ; {} ; {}\n".format(issue.key," ".join(servers[0] for servers in teste1), ",".join(link[0] for link in teste4), teste5+" "+ " ".join(time[0] for time in teste2), #teste3))

        csvfile.write("{} ; {} ; {} ; {} ; {} ; {}\n".format(issue.key, " ".join(p[0] for p in teste6)," ".join(servers[0] for servers in teste1), ",".join(link[0] for link in teste4), teste5+" "+ " ".join(time[0] for time in teste2), teste3))


        #--cert="C:\Users\guilherme.fagundes.NSXBR\Desktop\certificado_WG_NSX.crt"    